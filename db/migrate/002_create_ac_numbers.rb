class CreateAcNumbers < ActiveRecord::Migration
  def change
    create_table :ac_numbers do |t|
      t.string :ac_number
      t.string :context
      t.string :context_id
      t.string :context_url
      t.integer :xml_ts
      t.string :xml_urn
      t.string :xml_url
      t.string :item_status
      t.string :error_source
      t.string :error_text
      t.string :urn_status
      t.string :urn_req_id
      t.string :urn_value
    end
  end
end
