class CreateEprints < ActiveRecord::Migration
  def change
    create_table :eprints do |t|
      t.integer :eprint_id
      t.string :eprint_status
      t.string :eprint_type
      t.string :eprint_sperre
      t.string :eprint_einverstaendnis
      t.string :ac_number
      t.string :ac_number_status
      t.string :matr
      t.string :matr_status
      t.string :urn
      t.string :urn_status
      t.string :item_status
    end
  end
end
